package controller.processors;

import controller.Package;

import java.util.List;

public interface InputProcessor {

    /**
     * Process input lines.
     * @return processed lines list
     */
    List<Package> processInput();
}

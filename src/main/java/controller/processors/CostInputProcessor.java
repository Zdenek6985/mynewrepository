package controller.processors;

import controller.Package;

import java.util.ArrayList;
import java.util.List;

/**
 * Process input lines of costs and return them as List of {@link Package}
 */
public class CostInputProcessor implements InputProcessor {

    private final List<String> costInputLines;

    /**
     * Regex: 'Weight Cost'
     * Number weight is equal or bigger then zero.
     * Example values: '1.75 5.00', '3 75.50', '3.9 25.30'
     */
    private final static String LINE_INPUT_REGEX = "^\\d*(?:\\.\\d{1,3})? \\d+\\.\\d{2}";

    public CostInputProcessor(final List<String> costInputLines) {
        this.costInputLines = costInputLines;
    }

    @Override
    public List<Package> processInput() {
        final var costPacks = new ArrayList<Package>();

        for (final String line : costInputLines) {

            if (line.matches(LINE_INPUT_REGEX)) {
                final String[] values = line.split(" ");

                final Double weight = Double.valueOf(values[0]);
                final Double cost = Double.valueOf(values[1]);

                if (weight.equals(0D)) { // FIXME add to regex expression
                    System.err.printf("Bad input: %s Skipped ...\n", line);
                    continue;
                }

                costPacks.add(new Package(weight, null, cost));

            } else {
                System.err.printf("Bad input: %s Skipped ...\n", line);
            }
        }

        return costPacks;
    }
}

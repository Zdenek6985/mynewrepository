package controller.processors;

import controller.Package;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Process input lines of post codes and return them as List of {@link Package}
 */
public class PostCodeInputProcessor implements InputProcessor {

    private final List<String> postCodeInputLines;

    /**
     * Regex: 'Weight PostNumber'
     * Number weight is equal or bigger then zero.
     * Example values: '1.75 48796', '578.21 12478', '987 68712'
     */
    private final static String USER_INPUT_REGEX = "^\\d*(?:\\.\\d{1,3})? \\d{5}";

    public PostCodeInputProcessor(final List<String> inputPostCodes) {
        this.postCodeInputLines = inputPostCodes;
    }

    @Override
    public List<Package> processInput() {
        return postCodeInputLines.stream()
                .map(PostCodeInputProcessor::processPostCodeInputLine)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    /**
     * Process single line to single {@link Package} class.
     *
     * @param line input data
     * @return package of processed input data
     */
    public static Optional<Package> processPostCodeInputLine(final String line) {
        if (line.matches(USER_INPUT_REGEX)) {
            final String[] values = line.split(" ");

            final Double weight = Double.valueOf(values[0]);
            final Integer postCode = Integer.valueOf(values[1]);

            if (weight.equals(0D)) { // FIXME add to regex expression
                System.err.println("Bad input: " + line);
                Optional.empty();
            }

            return Optional.of(
                    new Package(weight, postCode));
        } else {
            System.err.println("Bad input: " + line);
            return Optional.empty();
        }
    }
}

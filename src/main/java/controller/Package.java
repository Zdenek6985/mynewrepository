package controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
public class Package {

    Double weight;
    Integer postCode;
    Double cost;

    public Package(Double weight, Integer postCode) {
        this.weight = weight;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return postCode + " " + weight + (cost != null ? " " + cost : "");
    }
}

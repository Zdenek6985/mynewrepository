package controller.threads;

import controller.Package;
import controller.constants.ExitCodes;

import java.util.List;

/**
 * Thread print loaded packages every minute.
 */
public class PrinterThread extends Thread {

    private final List<Package> packages;
    private final int WAIT_TIME_MILLIS = 60_000;

    public PrinterThread(final List<Package> packages) {
        this.packages = packages;
    }

    @Override
    public void run() {
        while(true) {
            packages.forEach(System.out::println);
            try {
                Thread.sleep(WAIT_TIME_MILLIS);
            } catch (final InterruptedException e) {
                System.err.println("Thread has been interrupted. Closing application. Reason: " + e);
                System.exit(ExitCodes.ERROR);
            }
        }
    }
}

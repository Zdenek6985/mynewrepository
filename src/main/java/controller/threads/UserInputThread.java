package controller.threads;

import controller.constants.ExitCodes;
import controller.processors.PostCodeInputProcessor;
import controller.Package;
import view.UserCommands;

import java.util.List;
import java.util.Scanner;

/**
 * Thread read user input, validate it and save it into memory.
 */
public class UserInputThread extends Thread {

    private final List<Package> packages;
    private final Scanner scanner;

    public UserInputThread(final List<Package> packages) {
        scanner = new Scanner(System.in);
        this.packages = packages;
    }

    @Override
    public void run() {
        while (true) {
            final String userInput = scanner.nextLine();

            if (userInput.equalsIgnoreCase(UserCommands.QUIT)) {
                System.out.println("User executed quit command. Closing application.");
                System.exit(ExitCodes.SUCCESS);
            }

            final var pack = PostCodeInputProcessor.processPostCodeInputLine(userInput);
            if(pack.isPresent()) {
                packages.add(pack.get());
            }
        }
    }
}

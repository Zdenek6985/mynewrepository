package controller;

import controller.constants.ExitCodes;
import controller.exceptions.FileProcessException;
import controller.processors.CostInputProcessor;
import controller.processors.PostCodeInputProcessor;
import controller.threads.PrinterThread;
import controller.threads.UserInputThread;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Load data and start threads.
 */
public class PackageController {

    private List<Package> packages;
    private UserInputThread userInputThread;
    private PrinterThread printerThread;

    public PackageController() {
        packages = Collections.synchronizedList(new ArrayList<>());
        userInputThread = new UserInputThread(packages);
        printerThread = new PrinterThread(packages);
    }

    public void start() {
        userInputThread.start();
        printerThread.start();
    }

    /**
     * Load data from file and start threads.
     *
     * @param postCodeFilePath data file with post codes
     */
    public void start(final String postCodeFilePath) {
        loadPostCodeData(postCodeFilePath);
        start();
    }

    /**
     * Load data from files and start threads.
     *
     * @param postCodeFilePath data file with post codes
     * @param costFilePath     data file with costs
     */
    public void start(final String postCodeFilePath, final String costFilePath) {
        loadPostCodeData(postCodeFilePath);
        addCostData(costFilePath);
        start();
    }

    private void addCostData(final String costFilePath) {
        try {
            final List<String> lines = Files.readAllLines(Path.of(costFilePath));
            final CostInputProcessor costInputProcessor = new CostInputProcessor(lines);
            final List<Package> costPacks = costInputProcessor.processInput();

            if (costPacks.size() != packages.size()) {
                throw new FileProcessException("Data files are not consistent");
            }

            for (int i = 0; i < packages.size(); i++) {
                if (!packages.get(i).getWeight().equals(costPacks.get(i).getWeight())) {
                    throw new FileProcessException("Data files are not consistent");
                }
                packages.get(i).setCost(costPacks.get(i).getCost());
            }
            System.out.printf("File %s processed.\n", costFilePath);
        } catch (final IOException e) {
            System.err.printf("Failed to load cost file. Reason: %s\n", e);
            System.exit(ExitCodes.ERROR);
        } catch (final FileProcessException e) {
            System.err.printf("Failed to process cost file. Program interrupted. Reason: %s\n", e);
            System.exit(ExitCodes.ERROR);
        }
    }

    private void loadPostCodeData(final String postCodeFilePath) {
        final List<String> lines;
        try {
            lines = Files.readAllLines(Path.of(postCodeFilePath));
            final PostCodeInputProcessor postCodeInputProcessor = new PostCodeInputProcessor(lines);
            packages.addAll(postCodeInputProcessor.processInput());
            System.out.printf("File %s loaded.\n", postCodeFilePath);
        } catch (final IOException e) {
            System.err.printf("Failed to load post code file. Reason: %s\n", e);
            System.exit(ExitCodes.ERROR);
        }
    }
}

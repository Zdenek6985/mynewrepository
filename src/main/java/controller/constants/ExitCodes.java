package controller.constants;

/**
 * Application exit codes.
 */
public class ExitCodes {
    public final static int SUCCESS = 0;
    public final static int ERROR = 1;
}

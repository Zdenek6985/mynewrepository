package controller.constants;

/**
 * Application arguments.
 */
public class ArgumentConstants {
    public static final String POST_CODE_FILE_PATH = "postCodeFilePath";
    public static final String COST_FILE_PATH = "costFilePath";
}

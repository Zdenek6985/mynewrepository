package controller.exceptions;

/**
 * Occurs when any problem with file processing appeared.
 */
public class FileProcessException extends Exception{

    public FileProcessException(String message) {
        super(message);
    }
}

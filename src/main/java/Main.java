import view.ArgumentsExtractor;
import controller.PackageController;

public class Main {

    public static void main(final String[] args) {

        final var argumentsExtractor = new ArgumentsExtractor(args);

        final var postCodeFilePath = argumentsExtractor.getPostCodeArgumentValue();
        final var costFilePath = argumentsExtractor.getCostArgumentValue();

        final PackageController controller = new PackageController();

        if (postCodeFilePath.isPresent() && costFilePath.isPresent()) {
            controller.start(postCodeFilePath.get(), costFilePath.get());
        } else if (postCodeFilePath.isPresent()) {
            controller.start(postCodeFilePath.get());
        } else {
            controller.start();
        }
    }

}

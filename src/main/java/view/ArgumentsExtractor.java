package view;

import controller.constants.ArgumentConstants;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.Optional;

/**
 * Extract argument values.
 */
public class ArgumentsExtractor {

    private static final String CMD_HELP_LINE_MESSAGE = "PackageDelivery";

    private CommandLine cmd;

    public ArgumentsExtractor(final String[] programArguments) {
        processArguments(programArguments);
    }

    /**
     * Extract post code file path value.
     * @return value of 'postCodeFilePath' argument
     */
    public Optional<String> getPostCodeArgumentValue() {
        return Optional.ofNullable(cmd.getOptionValue(ArgumentConstants.POST_CODE_FILE_PATH));
    }

    /**
     * Extract code file path value.
     * @return value of 'costFilePath' argument
     */
    public Optional<String> getCostArgumentValue() {
        return Optional.ofNullable(cmd.getOptionValue(ArgumentConstants.COST_FILE_PATH));
    }

    private void processArguments(final String[] programArguments) {
        final Options options = new Options();

        final Option postCodeFilePath = new Option(ArgumentConstants.POST_CODE_FILE_PATH, true, "post code file path");
        options.addOption(postCodeFilePath);

        final Option costFilePath = new Option(ArgumentConstants.COST_FILE_PATH, true, "cost file path");
        options.addOption(costFilePath);

        final CommandLineParser parser = new DefaultParser();

        try {
            cmd = parser.parse(options, programArguments);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            new HelpFormatter().printHelp(CMD_HELP_LINE_MESSAGE, options);
        }
    }
}

package controller.processors;

import controller.Package;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CostInputProcessorTest {

    private CostInputProcessor costInputProcessor;

    @Test
    public void processInput_Success() {
        costInputProcessor = new CostInputProcessor(List.of("4.45 5.40", "7.50 3.80", "10.6 22.60"));

        final List<Package> packages = costInputProcessor.processInput();

        final List<Package> excepted = List.of(
                new Package(4.45d, null, 5.4d),
                new Package(7.5d, null, 3.8d),
                new Package(10.6d, null, 22.6d)
        );

        Assert.assertEquals(excepted, packages);
    }

    @Test
    public void processInput_Fail() {
        costInputProcessor = new CostInputProcessor(List.of("0 0", "0.00 5.40", "7.5054 3.80", "-10.6 22.60"));

        Assert.assertEquals(List.of(), costInputProcessor.processInput());
    }
}
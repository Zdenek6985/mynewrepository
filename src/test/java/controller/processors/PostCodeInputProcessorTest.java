package controller.processors;

import controller.Package;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PostCodeInputProcessorTest {

    private PostCodeInputProcessor postCodeInputProcessor;

    @Test
    public void processInput_Success() {
        postCodeInputProcessor = new PostCodeInputProcessor(List.of("4.45 12345", "7.50 38415", "10.6 22601"));

        final List<Package> packages = postCodeInputProcessor.processInput();

        final List<Package> excepted = List.of(
                new Package(4.45d, 12345, null),
                new Package(7.5d, 38415, null),
                new Package(10.6d, 22601, null)
        );

        Assert.assertEquals(excepted, packages);
    }

    @Test
    public void processInput_Fail() {
        postCodeInputProcessor = new PostCodeInputProcessor(List.of("-4.45 12345", "7.50 3841", "10.6 422601",
                "-10.6 422601", "0.000 422601", "10.6 4226a"));


        Assert.assertEquals(List.of(), postCodeInputProcessor.processInput());
    }
}
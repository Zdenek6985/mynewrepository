package controller.threads;

import controller.Package;
import controller.constants.ExitCodes;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import view.UserCommands;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@RunWith(MockitoJUnitRunner.class)
public class UserInputThreadTest {

    private UserInputThread userInputThread;

    @Mock
    private Scanner scanner;

    private final SecurityManager defaultSystemSecurityManager = System.getSecurityManager();

    @Before
    public void setUp() throws Exception {
        userInputThread = new UserInputThread(new ArrayList<>());
        System.setSecurityManager(new NoExitTestSecurityManager());

        // set mock
        final Field scannerField = userInputThread.getClass()
                .getDeclaredField("scanner");
        scannerField.setAccessible(true);
        scannerField.set(userInputThread, scanner);
    }

    @After
    public void tearDown() {
        System.setSecurityManager(defaultSystemSecurityManager);
    }

    @Test
    public void run_userQuit() {
        Mockito.when(scanner.nextLine()).thenReturn(UserCommands.QUIT);
        try {
            userInputThread.run();
        } catch (ExitTestException ex) {
            Assert.assertEquals(ExitCodes.SUCCESS, ex.getStatus());
        }
    }

    @Test
    public void run_userInput() throws Exception {
        // set public packages field
        final var packages = userInputThread.getClass().getDeclaredField("packages");
        packages.setAccessible(true);

        // prepare mock
        final String WEIGHT = "5.507";
        final String POST_CODE = "45678";
        Mockito.when(scanner.nextLine()).thenReturn(WEIGHT + ' ' + POST_CODE).thenReturn(UserCommands.QUIT);

        try {
            userInputThread.run();
        } catch (ExitTestException ex) {

        }

        final var packageList = (List<Package>) packages.get(userInputThread);

        Assert.assertEquals(new Package(Double.valueOf(WEIGHT), Integer.valueOf(POST_CODE)), packageList.get(0));
    }


}
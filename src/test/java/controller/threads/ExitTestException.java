package controller.threads;

import lombok.Getter;

@Getter
public class ExitTestException extends SecurityException {
    private final int status;

    public ExitTestException(int status) {
        super("Custom exit test exception");
        this.status = status;
    }
}

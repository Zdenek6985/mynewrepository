package controller.threads;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;

public class PrinterThreadTest {

    private PrinterThread printerThread;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private final SecurityManager defaultSystemSecurityManager = System.getSecurityManager();

    @After
    public void restoreStreams() {
        System.setOut(standardOut);
        System.setSecurityManager(defaultSystemSecurityManager);
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        System.setSecurityManager(new NoExitTestSecurityManager());
        printerThread = new PrinterThread(Collections.synchronizedList(List.of(
                new controller.Package(5.5d, 12345, 4.50),
                new controller.Package(10.5d, 74586, 9.50)
        )));
    }

    @Test
    public void run() throws InterruptedException {
        printerThread.start();
        Thread.sleep(100);

        final String[] consoleOutput = outputStreamCaptor.toString().split("\r\n");
        Assert.assertEquals("12345 5.5 4.5", consoleOutput[0]);
        Assert.assertEquals("74586 10.5 9.5", consoleOutput[1]);
    }
}

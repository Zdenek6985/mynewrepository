package controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PackageControllerTest {

    private PackageController packageController;
    private Field packages;

    private File postCodeTempFile;
    private File costTempFile;

    private static final String WEIGHT_1 = "0.256";
    private static final String WEIGHT_2 = "5.45";
    private static final String WEIGHT_3 = "25.89";
    private static final String POST_CODE_1 = "12345";
    private static final String POST_CODE_2 = "45689";
    private static final String POST_CODE_3 = "65428";
    private static final String COST_1 = "5.70";
    private static final String COST_2 = "0.10";
    private static final String COST_3 = "123.35";

    @Before
    public void setUp() throws Exception {
        packageController = new PackageController();

        // set accessible packages
        packages = packageController.getClass().getDeclaredField("packages");
        packages.setAccessible(true);

        // create test data file
        postCodeTempFile = File.createTempFile("package_controller_post_code_test", "txt");
        final FileWriter myWriter = new FileWriter(postCodeTempFile);
        myWriter.write(WEIGHT_1 + ' ' + POST_CODE_1 + System.lineSeparator() +
                WEIGHT_2 + ' ' + POST_CODE_2 + System.lineSeparator() +
                WEIGHT_3 + ' ' + POST_CODE_3);
        myWriter.close();

    }

    @After
    public void tearDown() throws Exception {
        if (postCodeTempFile != null) {
            postCodeTempFile.delete();
        }
        if(costTempFile != null){
            costTempFile.delete();
        }
    }

    @Test
    public void loadPostCodeDataAndStart() throws Exception {
        packageController.start(postCodeTempFile.getAbsolutePath());

        final var packageList = (List<Package>) packages.get(packageController);
        Assert.assertEquals(3, packageList.size());
        Assert.assertEquals(new Package(Double.valueOf(WEIGHT_1), Integer.valueOf(POST_CODE_1)), packageList.get(0));
        Assert.assertEquals(new Package(Double.valueOf(WEIGHT_2), Integer.valueOf(POST_CODE_2)), packageList.get(1));
        Assert.assertEquals(new Package(Double.valueOf(WEIGHT_3), Integer.valueOf(POST_CODE_3)), packageList.get(2));
    }

    @Test
    public void loadPostCodeDataAndAddCostAndStart() throws Exception {
        costTempFile = File.createTempFile("package_controller_cost_test", "txt");
        final FileWriter myWriter = new FileWriter(costTempFile);
        myWriter.write(WEIGHT_1 + ' ' + COST_1 + System.lineSeparator() +
                WEIGHT_2 + ' ' + COST_2 + System.lineSeparator() +
                WEIGHT_3 + ' ' + COST_3);
        myWriter.close();

        packageController.start(postCodeTempFile.getAbsolutePath(), costTempFile.getAbsolutePath());

        final var packageList = (List<Package>) packages.get(packageController);
        Assert.assertEquals(3, packageList.size());
        Assert.assertEquals(new Package(Double.valueOf(WEIGHT_1), Integer.valueOf(POST_CODE_1), Double.valueOf(COST_1)), packageList.get(0));
        Assert.assertEquals(new Package(Double.valueOf(WEIGHT_2), Integer.valueOf(POST_CODE_2), Double.valueOf(COST_2)), packageList.get(1));
        Assert.assertEquals(new Package(Double.valueOf(WEIGHT_3), Integer.valueOf(POST_CODE_3), Double.valueOf(COST_3)), packageList.get(2));
    }

}
package view;


import controller.constants.ArgumentConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArgumentsExtractorTest {

    private static final String POST_CODE_FILE_PATH = "postCode/path/postCode.txt";
    private static final String COST_FILE_PATH = "cost/path/cost.txt";

    private ArgumentsExtractor argumentsExtractor;

    @Before
    public void setUp() {
        final String[] arguments = new String[4];
        arguments[0] = "-" + ArgumentConstants.POST_CODE_FILE_PATH;
        arguments[1] = POST_CODE_FILE_PATH;
        arguments[2] = "-" + ArgumentConstants.COST_FILE_PATH;
        arguments[3] = COST_FILE_PATH;

        argumentsExtractor = new ArgumentsExtractor(arguments);
    }

    @Test
    public void getPostCodeArgumentValue() {
        Assert.assertEquals(POST_CODE_FILE_PATH, argumentsExtractor.getPostCodeArgumentValue().orElseThrow());
    }

    @Test
    public void getCostArgumentValue() {
        Assert.assertEquals(COST_FILE_PATH, argumentsExtractor.getCostArgumentValue().orElseThrow());
    }
}
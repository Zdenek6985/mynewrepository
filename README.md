**Run from source code:**

1. Download and install OpenJDK 15

2. Clone repository to local

3. Open with Intellij IDEA

4. Run Maven goal (optional)

5. Run Main method

Not any problem should appear.

**Run JAR file:**

1. Download and install OpenJDK 15

2a. Run command 'java -jar PackageDelivery.jar'

2b: Run command 'C:\...\java.exe -jar PackageDelivery.jar'



**Program arguments:**

1. 'postCodeFilePath'

    Load post code data from file
    
    Example: 
    
    'java -jar PackageDelivery.jar -postCodeFilePath "C:\...\postCodes.txt"'

2. 'costFilePath'

    Load prices to loaded post codes
    
    Example: 
    
    'java -jar PackageDelivery.jar -postCodeFilePath "C:\...\postCodex.txt" -costFilePath "C:\...\costs.txt"'
    


